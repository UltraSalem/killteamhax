translate([-4.5,-1.5])
	{
	difference()
		{
		union()
		{
		translate([6,0])
			{
			cube([3,1,0.1]);
			}
			cube([6,3,0.1]);
		}
		translate([0,0,0.06])
		linear_extrude(height = 0.04)
		union()
			{
			translate([4.3,0.6])
				{ 
				text("9\"", font="Caladea", size = 0.5);
				}

			translate([8.8,0.2])
			rotate(90)
				{ 
				text("1\"", font="Caladea", size = 0.5);
				}

			translate([5.3,1.75])
			rotate(90)
				{ 
				text("2\"", font="Caladea", size = 0.5);
				}

			translate([0.6,1.7])
			rotate(270)
				{ 
				text("3\"", font="Caladea", size = 0.5);
				}

			translate([3.3,2.4])
			rotate(180)
				{ 
				text("6\"", font="Caladea", size = 0.5);
				}
			}
		}

	difference()
		{
		union()
			{
			linear_extrude(height = 0.15)
			polygon(points=[[1.4,1],[1.2,1.6],[4.4,1.6],[4.2,1]]);
			linear_extrude(height = 0.13)
			polygon(points=[[2.8,0.8],[2.1,1.9],[3.5,1.9]]);
			}
		translate([1.5,1.1,0.1])
			{ 
			linear_extrude(height = 0.05)
			text("KILL TEAM", font="Memo Std, Bold", size = 0.35);
			}
		};

	linear_extrude(height = 0.15)
		union()
		{
		polygon(points=[[1,0],[1.1,0.5],[0.9,0.5]]);
		polygon(points=[[2,0],[2.1,0.5],[1.9,0.5]]);
		polygon(points=[[3,0],[3.1,0.5],[2.9,0.5]]);
		polygon(points=[[4,0],[4.1,0.5],[3.9,0.5]]);
		polygon(points=[[5,0],[5.1,0.5],[4.9,0.5]]);
		polygon(points=[[6,0],[6.1,0.5],[5.9,0.5]]);
		polygon(points=[[7,0],[7.1,0.5],[6.9,0.5]]);
		polygon(points=[[8,0],[8.1,0.5],[7.9,0.5]]);
		polygon(points=[[0,1],[0.5,1.1],[0.5,0.9]]);
		polygon(points=[[0,2],[0.5,2.1],[0.5,1.9]]);
		polygon(points=[[1,3],[0.9,2.5],[1.1,2.5]]);
		polygon(points=[[2,3],[1.9,2.5],[2.1,2.5]]);
		polygon(points=[[3,3],[2.9,2.5],[3.1,2.5]]);
		polygon(points=[[4,3],[3.9,2.5],[4.1,2.5]]);
		polygon(points=[[5,3],[4.9,2.5],[5.1,2.5]]);
		polygon(points=[[6,2],[5.5,1.9],[5.5,2.1]]);
		}
	}
